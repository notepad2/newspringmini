package com.mini.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.mini.dto.CarDto;
import com.mini.model.Car;
import com.mini.repository.CarRepository;

@SpringBootTest
class CarServiceImplTest {

	@Autowired
	private  CarService carService;
	
	@MockBean
	private CarRepository carRepository;
	
	Car car;
	
	@BeforeEach
	void setUp() {
		Car car = new Car("Naveen", "20-10-23", "21-10-23", "Alto");
//		Mockito.when(carRepository.findAll()).thenReturn(Collections.singletonList(car));
	   String name1 = "Alto";
		Mockito.when(carRepository.findByNameOfCar(name1)).thenReturn(car);
	}
	
	@Test
	public void testAllCars() {
	
//	List<CarDto> cars	=carService.getAllCars();
//	assertEquals(1, cars.size());
//	assertEquals(car, cars.get(0));
		
		String name = "Alto";
		Car car = carService.getByName(name);
		
		assertEquals(name , car.getNameOfCar());
		
	}
}
