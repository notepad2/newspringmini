package com.mini.dto;

public class FlightDto {
	private long id;
	private String source;
	private String detination;
	private String name;
	private String email;
	private int numberOp;
	private String date;
	public FlightDto(long id, String source, String detination, String name, String email, int numberOp, String date) {
		super();
		this.id = id;
		this.source = source;
		this.detination = detination;
		this.name = name;
		this.email = email;
		this.numberOp = numberOp;
		this.date = date;
	}
	public FlightDto() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDetination() {
		return detination;
	}
	public void setDetination(String detination) {
		this.detination = detination;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getNumberOp() {
		return numberOp;
	}
	public void setNumberOp(int numberOp) {
		this.numberOp = numberOp;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "FlightDto [id=" + id + ", source=" + source + ", detination=" + detination + ", name=" + name
				+ ", email=" + email + ", numberOp=" + numberOp + ", date=" + date + "]";
	}
	
	

}
