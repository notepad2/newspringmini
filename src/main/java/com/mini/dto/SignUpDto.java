package com.mini.dto;

public class SignUpDto {

	private long id;
	private String firstName;
	private String password;
	private String email;
	public SignUpDto(long id, String firstName, String password, String email) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.password = password;
		this.email = email;
	}
	public SignUpDto() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "SignUpDto [id=" + id + ", firstName=" + firstName + ", password=" + password + ", email=" + email + "]";
	}
	
	
	
}
