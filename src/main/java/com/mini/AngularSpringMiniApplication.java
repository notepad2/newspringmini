package com.mini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularSpringMiniApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularSpringMiniApplication.class, args);
		
	}
}
