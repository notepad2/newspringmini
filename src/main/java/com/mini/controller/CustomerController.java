package com.mini.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mini.dto.BusDto;
import com.mini.dto.CarDto;
import com.mini.dto.FlightDto;
import com.mini.dto.SignUpDto;
import com.mini.model.Car;
import com.mini.service.Bus;
import com.mini.service.CarService;
import com.mini.service.FlightService;
import com.mini.service.SignUpService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/")
public class CustomerController {
	
	@Autowired
	private SignUpService signUpService;

	@Autowired
	private CarService carService;

	@Autowired
	private FlightService flightService;

	@Autowired
	private Bus bus; // BusService

	// Login Page
	@PostMapping("/login")
	public ResponseEntity<?> loginUsers(@RequestBody SignUpDto signUpDto ) {
		return signUpService.loginUsers(signUpDto);
	}

	// For Flight
	@PostMapping("/flight")
	public FlightDto createFlight(@RequestBody FlightDto flightDto) {
		FlightDto flightDto2 = flightService.saveDetails(flightDto);
		return flightDto2;
	}

	// SignUp Save Details
	@PostMapping("/customers")
	public SignUpDto createCustomer(@RequestBody SignUpDto signUpDto) {
		SignUpDto dto	=	signUpService.saveAllCustomers(signUpDto);
		return dto;
	}

	// For Cars
	@PostMapping("/car")
	public CarDto carDetails(@RequestBody CarDto carDto) {
		CarDto carDto2 = carService.carDetails(carDto);
		return carDto2;
	}

	// For Passengers Bus
	@PostMapping("/passengers")
	public BusDto passengerDetails(@RequestBody BusDto busDto) {

		BusDto savedPassengerDto = bus.passengerDetails(busDto);
		return savedPassengerDto;
	}

	// Getting Customers SignUp
	@GetMapping("/customers")
	public List<SignUpDto> getAllCustomers() {
			List<SignUpDto> list	=	signUpService.getAllCustomers();
			return list;
	}

	// Getting FlightBookings
	@GetMapping("/flight")
	public ResponseEntity<List<FlightDto>> getBookings() {
		List<FlightDto> flightDtos = flightService.getAllBookings();
		return ResponseEntity.ok(flightDtos);

	}

	// Get FlightBooking By Id
	@GetMapping("/flight/{id}")
	public ResponseEntity<FlightDto> getFlightDetailsById(@PathVariable Long id) {
		FlightDto flightDto = flightService.getDetailsById(id);
		return ResponseEntity.ok(flightDto);
	}

	// Update FlightBooking By Id

	@PutMapping("flight/{id}")
	public ResponseEntity<FlightDto> getFlightBookings(@PathVariable Long id, @RequestBody FlightDto flightBdetail) {
		FlightDto flightDto = flightService.UpdateFlightBooking(id, flightBdetail);
		return ResponseEntity.ok(flightDto);
	}

	// Delete By ID
	@DeleteMapping("flight/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteFlightByID(@PathVariable Long id) {
		Map<String, Boolean> returnresponse = flightService.deleteBookById(id);
		return ResponseEntity.ok(returnresponse);
	}

	// Get all Car Booking Details
	@GetMapping("/car")
	public ResponseEntity<List<CarDto>> getCarRentals() {
		List<CarDto> carDtos = carService.getAllCars();
		return ResponseEntity.ok(carDtos);
	}

	// Get Car Rentals by id
	@GetMapping("car/{id}")
	public ResponseEntity<CarDto> getCarRentalsByid(@PathVariable("id") Integer getById) {
		CarDto carDto = carService.getCarBookerById(getById);
		return ResponseEntity.ok(carDto);
	}

	// Delete CarBookingRentals By ID
	@DeleteMapping("car/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteCarBooking(@PathVariable("id") Integer delById) {
		Map<String, Boolean> map = carService.deleteById(delById);
		return ResponseEntity.ok(map);
	}

	// Updating CarBooking Rentals By ID
	@PutMapping("/car/{id}")
	public ResponseEntity<CarDto> updateCarRentals(@PathVariable Integer id, @RequestBody CarDto carDtoUpdate) {
		CarDto carDto = carService.updateDetailsById(id, carDtoUpdate);
		return ResponseEntity.ok(carDto);
	}
	
	//GetByCarName
	
	@GetMapping("getByName/{name}")
	public Car getByName(@PathVariable String name ) {
		Car car	=	carService.getByName(name);
			return car;
	}	
}
