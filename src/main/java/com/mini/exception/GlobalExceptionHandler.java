package com.mini.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {
		
	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED )
	@ResponseBody
	public ResponseEntity<String> handleCustomxception(ResourceNotFoundException exception)
	{
		String expMessage = exception.getMessage();
		return new ResponseEntity<>(expMessage, HttpStatus.UNAUTHORIZED );
	}
}
