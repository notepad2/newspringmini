package com.mini.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.mini.dto.SignUpDto;
import com.mini.exception.ResourceNotFoundException;
import com.mini.model.Customer;
import com.mini.repository.CustomerRepository;

@Service
public class SignUpServiceImpl implements SignUpService {

	@Autowired
	private CustomerRepository customerRepository;

	ConvertTo convertTo = new ConvertTo();

	@Override
	public SignUpDto saveAllCustomers(SignUpDto signUpDto) {

		Customer customer = convertTo.changeToCustomer(signUpDto);
		Customer customer2 = customerRepository.save(customer);
		SignUpDto signDto = convertTo.changeToDto(customer2);
		return signDto;
	}

	@Override
	public List<SignUpDto> getAllCustomers() {

		List<Customer> customers = customerRepository.findAll();
		List<SignUpDto> list = new ArrayList<>();
		for (Customer customer : customers) {
			SignUpDto signUpDto = new SignUpDto();
			signUpDto.setId(customer.getId());
			signUpDto.setFirstName(customer.getFirstName());
			signUpDto.setPassword(customer.getPassword());
			signUpDto.setEmail(customer.getEmail());

			list.add(signUpDto);
		}
		return list;
	}

	@Override
	public ResponseEntity<?> loginUsers(SignUpDto signUpDto) {

		Customer customerdata = convertTo.changeToCustomer(signUpDto);
		
		Customer cs = customerRepository.findByFirstName(customerdata.getFirstName());
		if (cs == null) {
			throw new ResourceNotFoundException(customerdata.getFirstName() + " : Entered Name Not Exists");
		}
		if (!cs.getPassword().equalsIgnoreCase(customerdata.getPassword())) {
			throw new ResourceNotFoundException("Entered Password Is Invalid , Please Enter Correct Password");
		}

		if (cs.getPassword().equals(customerdata.getPassword())) {
			return ResponseEntity.ok(cs);
		}

		return (ResponseEntity<?>) ResponseEntity.internalServerError();
	}
}

@Component // Save SignUp and for login
class ConvertTo {
	public Customer changeToCustomer(SignUpDto dto) {
		Customer customer = new Customer();

		customer.setId(dto.getId());
		customer.setFirstName(dto.getFirstName());
		customer.setEmail(dto.getEmail());
		customer.setPassword(dto.getPassword());
		return customer;
	}

	public SignUpDto changeToDto(Customer customer) {

		SignUpDto dto = new SignUpDto();

		dto.setId(customer.getId());
		dto.setFirstName(customer.getFirstName());
		dto.setPassword(customer.getPassword());
		dto.setEmail(customer.getEmail());
		return dto;
	}
}
