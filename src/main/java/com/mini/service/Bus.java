package com.mini.service;

import org.springframework.stereotype.Service;

import com.mini.dto.BusDto;


@Service
public interface Bus {
	 public BusDto passengerDetails( BusDto busDto );
}
