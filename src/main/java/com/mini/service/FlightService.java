package com.mini.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mini.dto.FlightDto;

@Service
public interface FlightService {

	FlightDto saveDetails(FlightDto flightDto);

	List<FlightDto> getAllBookings();

	FlightDto getDetailsById(Long id);

	FlightDto UpdateFlightBooking(Long id, FlightDto flightBdetail);

	Map<String, Boolean> deleteBookById(Long id);

}
