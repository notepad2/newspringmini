package com.mini.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mini.dto.SignUpDto;

@Service
public interface SignUpService {

	SignUpDto saveAllCustomers(SignUpDto signUpDto);

	List<SignUpDto> getAllCustomers();
 ResponseEntity<?> loginUsers( SignUpDto signUpDto );

}
