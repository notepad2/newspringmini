package com.mini.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.mini.dto.FlightDto;
import com.mini.exception.ResourceNotFoundException;
import com.mini.mapper.Mapper;
import com.mini.model.Flight;
import com.mini.repository.FlightRepository;

@Service
public class FlightServiceImpl implements FlightService {

	@Autowired
	private FlightRepository flightRepository;

	@Autowired
	private JavaMailSender javaMailSender;

	public FlightDto saveDetails(FlightDto flightDto) {

		Flight flight = Mapper.mapToFlight(flightDto);
		Flight flight2 = flightRepository.save(flight);
		FlightDto flightDto2 = Mapper.mapToFlightDto(flight2);
		
		String name = flightDto.getName();
		String name1 ="Name : ";
		String nameconcat =name1.concat(name);
		
		String mail = flightDto.getEmail();
		String source= flightDto.getSource();
		String destination = flightDto.getDetination();
		String date = flightDto.getDate();
																						
		String combine = nameconcat.concat("  ").concat("Your Email : "+mail+"  ").concat("").concat("Location : "+source+"  ").concat("  To  ").concat("Destination : "+destination+"  ").concat("").concat("Date of travel  : "+date);
		sendEmail("kaarthira03@gmail.com",mail , "FlightBooking Details", "Booking Successful and Your Details are :"+combine) ;
		return flightDto2;
	}

	@Override
	public List<FlightDto> getAllBookings() {
		List<Flight> flights = flightRepository.findAll();
		return flights.stream().map((flightDto) -> Mapper.mapToFlightDto(flightDto)).collect(Collectors.toList());
	}

	@Override
	public FlightDto getDetailsById(Long id) {

		Flight flight = flightRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Id Not Found : " + id));
		return Mapper.mapToFlightDto(flight);
	}

	@Override
	public FlightDto UpdateFlightBooking(Long id, FlightDto flightBdetail) {

		Flight flight = flightRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Id Not Found :" + id));

		flight.setSource(flightBdetail.getSource());
		flight.setDetination(flightBdetail.getDetination());
		flight.setName(flightBdetail.getName());
		flight.setEmail(flightBdetail.getEmail());
		flight.setNumberOp(flightBdetail.getNumberOp());
		flight.setDate(flightBdetail.getDate());

		Flight flight2 = flightRepository.save(flight);

		return Mapper.mapToFlightDto(flight2);

	}

	@Override
	public Map<String, Boolean> deleteBookById(Long id) {

		Flight flight = flightRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Id Not " + id + "Found"));
		flightRepository.delete(flight);

		Map<String, Boolean> res = new LinkedHashMap<>();
		res.put("Deleted", Boolean.TRUE);
		return res;
	}

	public void sendEmail(String fromEmail, String toEmai, String heading, String body) {
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
				
		mailMessage.setFrom(fromEmail);
		mailMessage.setTo(toEmai);
		mailMessage.setSubject(heading);
		mailMessage.setText(body);

		javaMailSender.send(mailMessage);
	}
}
