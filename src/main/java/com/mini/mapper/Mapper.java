package com.mini.mapper;

import com.mini.dto.BusDto;
import com.mini.dto.CarDto;
import com.mini.dto.FlightDto;
import com.mini.model.Car;
import com.mini.model.Flight;
import com.mini.model.Passenger;

public class Mapper {
	
	public static Passenger mapTopassenger(BusDto busDto )
	{
		return new Passenger(
				busDto.getId(),
				busDto.getFrm(),
				busDto.getTom(),
				busDto.getNop(),
				busDto.getName(),
				busDto.getEmail()
				);
	}
	
	public static BusDto mapToBusDto(Passenger passenger )
	{
		return new BusDto(
				passenger.getId(),
				passenger.getFrm(),
				passenger.getTom(),
				passenger.getNop(),
				passenger.getName(),
				passenger.getEmail()
				);
	}
	
	public static Car mapToCar(CarDto carDto ) {
		return new Car(
				carDto.getId(),
				carDto.getNameOfCS(),
				carDto.getFromDate(),
				carDto.getToDate(),
				carDto.getNameOfCar()
				);
	}
	public static CarDto mapToCarDto(Car car ) {
		return new CarDto(
				
				car.getId(),
				car.getNameOfCS(),
				car.getFromDate(),
				car.getToDate(),
				car.getNameOfCar()
				);	
	}
	
	public static Flight mapToFlight(FlightDto flightDto ) {
		return new Flight(
				flightDto.getId(),
				flightDto.getSource(),
				flightDto.getDetination(),
				flightDto.getName(),
				flightDto.getEmail(),
				flightDto.getNumberOp(),
				flightDto.getDate()
				);
	}
	public static FlightDto mapToFlightDto(Flight flight) {
		return new FlightDto( 
				flight.getId() , 
				flight.getSource() , 
				flight.getDetination()  , 
				flight.getName(),
				flight.getEmail(),
				flight.getNumberOp() ,
				flight.getDate()
				);
	}
}
