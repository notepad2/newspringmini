package com.mini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mini.model.Flight;

public interface FlightRepository extends JpaRepository<Flight, Long> {

}
