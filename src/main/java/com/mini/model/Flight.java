package com.mini.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name ="Flight")
public class Flight {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="source")
	private String source;
	
	@Column(name = "detination")
	private String detination;
	
	@Column( name = "Name")
	private String name;
	
	@Column(name = "Email")
	private String email;
	
	@Column(name="NumberOp")
	private int numberOp;
	
	@Column(name = "date")
	private String date;

	public Flight(long id, String source, String detination, String name, String email, int numberOp, String date) {
		super();
		this.id = id;
		this.source = source;
		this.detination = detination;
		this.name = name;
		this.email = email;
		this.numberOp = numberOp;
		this.date = date;
	}

	public Flight() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDetination() {
		return detination;
	}

	public void setDetination(String detination) {
		this.detination = detination;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNumberOp() {
		return numberOp;
	}

	public void setNumberOp(int numberOp) {
		this.numberOp = numberOp;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Flight [id=" + id + ", source=" + source + ", detination=" + detination + ", name=" + name + ", email="
				+ email + ", numberOp=" + numberOp + ", date=" + date + "]";
	}
}
