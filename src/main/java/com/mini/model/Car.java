package com.mini.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table( name = "car")
public class Car {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private int id ;
	
	@Column(name=" Name Of Customer")
	private String nameOfCS;	
	
	@Column(name = "From-date")
	private String fromDate ;
	
	@Column(name="To-Date")
	private String toDate ;
	
	@Column(name="Name Of The Car")
	private String nameOfCar ;
	public Car(int id, String nameOfCS, String fromDate, String toDate, String nameOfCar) {
		super();
		this.id = id;
		this.nameOfCS = nameOfCS;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.nameOfCar = nameOfCar;
	}
	public Car(String nameOfCS, String fromDate, String toDate, String nameOfCar) {
		super();
		this.nameOfCS = nameOfCS;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.nameOfCar = nameOfCar;
	}
	public Car() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNameOfCS() {
		return nameOfCS;
	}
	public void setNameOfCS(String nameOfCS) {
		this.nameOfCS = nameOfCS;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getNameOfCar() {
		return nameOfCar;
	}
	public void setNameOfCar(String nameOfCar) {
		this.nameOfCar = nameOfCar;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", nameOfCS=" + nameOfCS + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", nameOfCar=" + nameOfCar + "]";
	}
}
